// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.
// 
// Requirement functions
//
// @author Dr Joseph Bater <joseph.baxter@nottingham.ac.uk>
// @copyright Copyright (c) 2017 The University of Nottingham
//

// Tooltip.
$(function() {
  $(document).tooltip();
});
// Load config page.
function go_config() {
  window.location='../admin/config.php';
}
// Validate ldap options, and toggle extra settings.
$(function () {
  $("#installForm").validate();
  $('#useLdap').change(function() {
    $('#ldapOptions').toggle();
  });
  $('#uselookupLdap').change(function() {
    $('#ldaplookupOptions').toggle();
  });
});