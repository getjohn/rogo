<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

$string['npmsuccess'] = 'NPM installation up to date.';
$string['composersuccess'] = 'Composer installation up to date.';
$string['phpversion'] = 'PHP version %s or above is required';
$string['phpsuccess'] = 'PHP version meets requirements';
$string['dbversion'] = 'MYSQL version %s or above is required';
$string['dbsuccess'] = 'MySQL version meets requirements';
$string['phpextension'] = 'The PHP extension %s is required';
$string['phpextensionsuccess'] = 'The PHP extension %s is enabled';
$string['help'] = 'Not all requirements met. Please refer to the <a href = "https://rogo-eassessment-docs.atlassian.net">documentation</a> on how to install any missing requirements.';
$string['langpacksmissing'] = 'Translation pack for language code "%s" missing.';
$string['langpacksfound'] = 'Translation packs installed.';