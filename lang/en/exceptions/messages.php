<?php
$string['filenotfound'] = 'File % was not found.';
$string['directorynotfound'] = 'Directory % was not found.';
$string['invalidfilepath'] = 'Invalid file path.';
