<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

$string['savefailattempts'] = 'Save Fail Attempts';
$string['student'] = 'Student';
$string['paper'] = 'Paper';
$string['client'] = 'Client';
$string['datetime'] = 'Date/Time';
$string['screen'] = 'Screen';
$string['status'] = 'Status';
$string['request'] = 'Request URL';
$string['response'] = 'Response Data';
$string['confirm_clear_all_logs'] = 'Are you sure you want delete all the logs?';
$string['confirm_clear_a_log'] = 'Are you sure you want delete this log?';
$string['clear_all_button_text'] = 'Clear all logs';
$string['icon_msg'] = 'Delete log';
?>